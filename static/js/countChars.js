function loop() {
    setTimeout(function() {
        console.log('loop');
        var maybeSlow = countChars();
        if (!maybeSlow) {
            loop();
        }
    }, 600000);
}

setTimeout(function() {
    var maybeSlow = countChars();
    if (!maybeSlow) {
        loop();
    }
}, 30000);

/*
 * The count method, trim and _decode methods are from countable.js (http://sacha.me/Countable/)
 */

/**
 * `String.trim()` polyfill for non-supporting browsers. This is the
 * recommended polyfill on MDN.
 *
 * @see     <http://goo.gl/uYveB>
 * @see     <http://goo.gl/xjIxJ>
 *
 * @return  {String}  The original string with leading and trailing
 *                    whitespace removed.
 */
if (!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, '')
  }
}

/**
 * `ucs2decode` function from the punycode.js library.
 *
 * Creates an array containing the decimal code points of each Unicode
 * character in the string. While JavaScript uses UCS-2 internally, this
 * function will convert a pair of surrogate halves (each of which UCS-2
 * exposes as separate characters) into a single code point, matching
 * UTF-16.
 *
 * @see     <http://goo.gl/8M09r>
 * @see     <http://goo.gl/u4UUC>
 *
 * @param   {String}  string   The Unicode input string (UCS-2).
 *
 * @return  {Array}   The new array of code points.
 */
function _decode (string) {
    var output = [],
            counter = 0,
            length = string.length,
            value, extra

    while (counter < length) {
        value = string.charCodeAt(counter++)

        if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
            // High surrogate, and there is a next character.
            extra = string.charCodeAt(counter++)

            if ((extra & 0xFC00) == 0xDC00) {
                // Low surrogate.
                output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000)
            } else {
                // unmatched surrogate; only append this code unit, in case the next
                // code unit is the high surrogate of a surrogate pair
                output.push(value, extra)
                counter--
            }
        } else {
            output.push(value)
        }
    }

    return output
}

function countChars() {
    if (typeof(window.warnTooMuchCharsEditorInfo) !== 'undefined') {
        text = window.warnTooMuchCharsEditorInfo.editor.exportText();
        /**
         * The initial implementation to allow for HTML tags stripping was created
         * @craniumslows while the current one was created by @Rob--W.
         *
         * @see <http://goo.gl/Exmlr>
         * @see <http://goo.gl/gFQQh>
         */
        text = text.replace(/<\/?[a-z][^>]*>/gi, '');

        var trimmed = text.trim();

        /**
         * Most of the performance improvements are based on the works of @epmatsw.
         *
         * @see <http://goo.gl/SWOLB>
         */
        var characters = _decode(text).length;

        if (characters > 150000) {
            $.gritter.add({
                class_name: 'warn_too_much_chars',
                title:window._('ep_warn_too_much_chars.close'),
                text: window._('ep_warn_too_much_chars.warning'),
                sticky: true,
            });
            return true;
        } else {
            return false;
        }
    }
}
